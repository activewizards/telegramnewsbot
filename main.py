#!/usr/bin/env python
import os
import config
import requests
import tornado.httpserver
import tornado.ioloop
import tornado.web
from twx.botapi import TelegramBot, InlineKeyboardMarkup, InlineKeyboardButton, answer_callback_query

bot = TelegramBot(config.BOT_TOKEN)


# response for /start
def start_message(arguments, message):
    text = "(｡◕‿‿◕｡) Hey, %s!" % message["from"].get("first_name")
    text += "\r\nI can show you the focus, just type /help"

    res = bot.send_message(message['chat']['id'], text).wait()
    print(res)


# response for /help
def help_message(arguments, message):
    text = ["Hey, %s!" % message["from"].get("first_name"),
              "\r\nI can accept only these commands:"]

    for command, val in CMD.items():
        text.append(command + " - " + val['desc'])

    text = "\n\t".join(text)
    res = bot.send_message(message['chat']['id'], text).wait()
    print(res)


def no_news_message(message):
    res = bot.send_message(
        message['chat']['id'],
        "¯\_(ツ)_/¯ I don't found any news! Try another request or type /help for more info."
    ).wait()
    print(res)


def sections_id_message(arguments, message):
    r = guardian_request('sections')
    sections = []
    for res in r['results']:
        sections.append(res['id'])

    text = ", ".join(sections)
    res = bot.send_message(message['chat']['id'], "Here category ID that I know: " + text).wait()
    print(res)


def sections_message(arguments, message):
    length = 4
    if len(arguments) == 0:
        start = 0
    else:
        start = int(arguments[0])

    r = guardian_request('sections')
    result = r['results'][start:(start + length)]
    keyboard = []

    if start > 0:
        keyboard.append([InlineKeyboardButton("Show perv", callback_data='/sections ' + str(start - length))])

    temp_array = []
    for res in result:
        button = InlineKeyboardButton(res['webTitle'], callback_data='/newest ' + res['id'])
        temp_array.append(button)
        if len(temp_array) == 2:
             keyboard.append(temp_array)
             temp_array = []

    if len(temp_array) > 0:
        keyboard.append(temp_array)

    if start + length < len(r['results']):
        keyboard.append([InlineKeyboardButton("Show more", callback_data='/sections ' + str(start + length))])

    reply_markup = InlineKeyboardMarkup(keyboard)
    res = bot.send_message(message['chat']['id'], "Here category that I know:", reply_markup=reply_markup).wait()
    print(res)


def newest_message(arguments, message):
    if not arguments:
        r = guardian_request('search', {'page': 1, 'page-size': 3})
        if r['status'] == 'error':
            no_news_message(message)
            return

        if r['total'] > 0:
            bot.send_message(message['chat']['id'], "I found a newest articles for You").wait()
            for res in r['results']:
                bot.send_message(
                    message['chat']['id'],
                    "Section: " + res['sectionName'] + "\r\n" + res['webUrl']
                ).wait()
        else:
            no_news_message(message)
    else:
        r = guardian_request('search', {'page': 1, 'page-size': 3, 'section': arguments[0]})
        if r['status'] == 'error':
            no_news_message(message)
            return

        if r['total'] > 0:
            bot.send_message(
                message['chat']['id'],
                "I found a newest articles for You in " + arguments[0] + " category"
            ).wait()
            for res in r['results']:
                bot.send_message(
                    message['chat']['id'],
                    res['webUrl']
                ).wait()
        else:
            no_news_message(message)


def guardian_request(action, params={}):
    rParams = {'api-key': config.GUARDIAN_API_KEY}
    rParams.update(params)
    r = requests.get(config.GUARDIAN_URL + "/" + action, rParams)
    print(str(r.status_code) + " / " + r.content.decode("utf-8"))
    return r.json()['response']


def mirror(message):
    text = "Hey, %s!" % message["from"].get("first_name")
    text += "\r\n" + message["text"]
    res = bot.send_message(message['chat']['id'], text).wait()
    print(res)


not_found = help_message
CMD = {
    '/start': {'action': start_message, 'desc': 'Start action'},
    '/help': {'action': help_message, 'desc': 'Find help and list available commands'},
    '/newest': {
        'action': newest_message,
        'desc': 'Get 3 latest news, or if /newest <section_id> - get latest in section'
    },
    '/sections': {'action': sections_message, 'desc': 'Get available section'},
    '/section_ids': {'action': sections_id_message, 'desc': 'Get available section ids'}
}


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.redirect(config.BOT_URL)


class SetWebhookHandler(tornado.web.RequestHandler):
    def get(self):
        bot.set_webhook(config.MyURL)
        self.write("It's ok")


class HookHandler(tornado.web.RequestHandler):
    def post(self):
        update = tornado.escape.json_decode(self.request.body)
        print(update)
        if 'message' in update:
            message = update['message']
            text = message.get('text')
            if text:
                print("MESSAGE\t%s\t%s" % (message['chat']['id'], text))
                # its a command
                if text[0] == '/':
                    command, *arguments = text.split(" ", 1)
                    cmd = CMD.get(command)
                    if not cmd:
                        not_found(arguments, message)
                    else:
                        cmd['action'](arguments, message)
                else:
                    # mirror(message)
                    help_message('', message)
        elif 'callback_query' in update:
            callback_query = update['callback_query']
            message = callback_query['message']
            data = callback_query.get('data')
            if data:
                print("MESSAGE\t%s\t%s" % (message['chat']['id'], data))
                command, *arguments = data.split(" ", 1)
                cmd = CMD.get(command)
                if not cmd:
                    not_found(arguments, message)
                else:
                    cmd['action'](arguments, message)
                acq_res = answer_callback_query(callback_query['id'], token=config.BOT_TOKEN)
                print(acq_res)


def main():
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/setWebhook", SetWebhookHandler),
        (r"/b39ba3aa968128b0daeab0ebf9a1eb75", HookHandler),
    ])
    http_server = tornado.httpserver.HTTPServer(application)
    port = int(os.environ.get("PORT", 5000))
    http_server.listen(port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
