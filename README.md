# README #

### Telegram API ###

Telegram accepts GET and/or POST requests for the url like:


```
#!python

https://api.telegram.org/bot<TOKEN>/<METHOD>
```

We can easily get the Token through the @BotFather. As well as it manages all bot configurations
```
#!python

@BotFather
```


Here is the list of available methods and parameters - https://core.telegram.org/bots/api#available-methods

To use WebHook it is imperative to use https (secure connection).

If Telegram did not receive ОК200 from the Webhook, it will call it again at intervals of half a second

### APP ###

Available list of commands:

```
#!python

CMD = {
    '/start': start_message,
    '/help': help_message,
    '/newest': newest_message,
    '/sections': sections_message,
    '/section_ids': sections_id_message
}
```

The command /newest can take a parameter = SectionsID

The command /sectionswith the help of  simple buttons, which in turn cause or /newest <section_id> or  /sections <start_num>

### TODO ###

At the moment, category lists with new messages. You can change / update the message through

https://core.telegram.org/bots/api#updating-messages

thus not post them in the tape dozens of messages by category
